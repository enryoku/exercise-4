﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Exercise_4
{
    public partial class timeCalculator : Form
    {
        public timeCalculator()
        {
            InitializeComponent();
        }

        private void ConvertButton_Click(object sender, EventArgs e)
        {
            int secondsToConvert;
            if (int.TryParse(secondsInput.Text, out secondsToConvert))
            {
                if (secondsToConvert < 60)
                {
                    timeOutput.Text = secondsToConvert.ToString() + " second(s)";
                }
                if (secondsToConvert >= 60 && secondsToConvert < 3600)
                {
                    secondsToConvert = secondsToConvert / 60;
                    timeOutput.Text = secondsToConvert.ToString() + " minute(s)";
                }
                if (secondsToConvert >= 3600 && secondsToConvert < 86400)
                {
                    secondsToConvert = secondsToConvert / 3600;
                    timeOutput.Text = secondsToConvert.ToString() + " hour(s)";
                }
                if (secondsToConvert >= 86400)
                {
                    secondsToConvert = secondsToConvert / 86400;
                    timeOutput.Text = secondsToConvert.ToString() + " day(s)";
                }
            }
            else
            {
                MessageBox.Show("Please enter a valid number of Seconds to convert! I was not able to convert " + secondsInput.Text + " to an integer.");
            }
        }

        private void ResetButton_Click(object sender, EventArgs e)
        {
            secondsInput.Text = null;
            timeOutput.Text = null;
        }
    }
}
